header = '''
<div class="container-fluid">
	<div class='page-header'>
        <h1 class="text-center" style="font-size: 3em;"><strong> {{name}} </strong></h1>
	</div>
	<div class="row">
		<div class="col-xs-12">
			<p class="text-center" style="font-size: 1.25em; color: #666">
				{{desc}} 
			</p>
		</div>
	</div>
'''

feature_left = '''
	<div class="row">
		<div class="col-xs-12 col-md-6 text-center">
			<div class="">
				<br>
				<h3 class="text-primary" style="font-size: 1.75em"> {{name}} </h3>
				<br>
				<p style="font-size: 1.25em; color: #222"> {{text}} </p>
			</div>
		</div>
		<div class="col-md-6 hidden-xs hidden-sm">
			<img class="img-responsive center-block" style="height: atuo" src="https:{{img}}">
		</div>
	</div>
'''

feature_right = '''
	<div class="row">
		<div class="col-md-6 hidden-xs hidden-sm">
			<img class="img-responsive center-block" style="height: atuo" src="https:{{img}}">
		</div>
		<div class="col-xs-12 col-md-6 text-center">
			<div class="">
				<br>
				<h3 class="text-primary" style="font-size: 1.75em"> {{name}} </h3>
				<br>
				<p style="font-size: 1.25em; color: #222"> {{text}} </p>
			</div>
		</div>
	</div>
'''

spec_header = '''
	<div class="row">
		<div class="col-xs-12">
			<h2 style="font-size: 1.75em;">{{name}}</h2>
			<div class="table-responsive">
				<table class="table">
					<tbody>
'''

spec_row = '''
						<tr>
							<th class="text-right" style="width: 30%"><strong>{{key}}</strong></th>
							<td class="text-left" style="width: 70%">{{value}}</td>
						</tr>
'''

spec_footer = '''
					</tbody>
				</table>
			</div>
		</div>
	</div>
'''

spec_title = '''
	<div class="row">
		<h1 style="font-size: 2em" class="text-center">Dane techniczne</h1>
	</div>
'''
