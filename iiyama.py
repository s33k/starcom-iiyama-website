from producer_parser import ProducerParser
from bs4 import BeautifulSoup
from iiyama_html import *


class Iiyama(ProducerParser):

    def _generate(self, content):
        device_name = ''
        desc = ''
        features = []
        specs = {}

        soup = BeautifulSoup(content, 'html.parser')

        device_name = soup.find("div", attrs={"class": "leadtxt"}).find('h3').get_text()
        print(device_name)
        if device_name is not None:
            device_name = device_name.replace("\n", "").replace("\t", "")
        else:
            device_name = ''
        desc = soup.find('div', attrs={"class": "x-description"})

        if device_name is not None:
            self.filename = device_name

        features_div = soup.find(id='featureslider')
        if features_div is None:
            features_div = soup.find(id="features")

        if features_div is not None:
            features_list = []
            features_list_div = features_div.find(attrs={"class": "slides"})
            if features_list_div is None:
                features_list = features_div.find("div", attrs={"class": "imageFeatures"}).find_all("div", attrs={"class": "row"})
            else:
                features_list = features_list_div.children
            for feature in features_list:
                if feature != "\n" and feature is not None:
                    feature_name = feature.find('h4')
                    if feature_name is None:
                        feature_name = feature.find('h3')
                        if feature_name is None:
                            feature_name = ""

                    if feature_name != "":
                        feature_name = feature_name.string
                    print(feature_name)
                    feature_text = feature.find('p').string
                    feature_img_div = feature.find('img')
                    feature_img = feature_img_div['src']
                    features.append({"name": feature_name, "text": feature_text, "img": feature_img})

        specification_divs = soup.find_all(attrs={'class': 'speci'})
        if specification_divs is not None and specification_divs != []:
            for specification_section in specification_divs:
                spec_name = specification_section.find('h4').a.string
                rows = specification_section.find_all('tr')
                rows_map = {}
                for row in rows:
                    row_key = row.find(attrs={'align': 'right'}).strong.string
                    row_value = row.find_all('td')[1]
                    row_value = row_value.get_text()
                    if row_value is not None:
                        row_value = row_value.__str__().replace("\t", "").replace("\n", "")
                    else:
                        row_value = ""
                    rows_map[row_key] = row_value
                specs[spec_name] = rows_map

        output = header
        output = output.replace("{{name}}", device_name)
        output = output.replace("{{desc}}", desc.__str__())

        index = 0
        for feature in features:
            if index % 2 == 0:
                output += feature_left
            else:
                output += feature_right
            output = output.replace("{{name}}", feature['name'])
            output = output.replace("{{text}}", feature['text'])
            output = output.replace("{{img}}", feature['img'])
            index += 1

        output += spec_title

        for spec_key in specs.keys():
            output += spec_header
            output = output.replace("{{name}}", spec_key)
            for spec_row_key in specs[spec_key].keys():
                output += spec_row
                output = output.replace("{{key}}", spec_row_key)
                output = output.replace("{{value}}", specs[spec_key][spec_row_key])
            output += spec_footer

        output += "</div>"
        return output
