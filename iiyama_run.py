from tkinter import *
from tkinter.ttk import Progressbar
from iiyama import Iiyama


def generate():
    progress['value'] = 0
    iiyama = Iiyama(link_field.get())
    iiyama.load()
    progress['value'] = 100


window = Tk()
window.resizable(width=False, height=False)
window.geometry('400x105')
window.title("Monitory :3  wersja iiyama")

title_label = Label(window, text="iiyama", font="Arial 10 bold")
title_label.grid(column=0, row=0, sticky=W)

link_label = Label(window, text="Link do strony monitora")
link_label.grid(column=0, row=1, columnspan=2, sticky=W)

link_field = Entry(window, width=30)
link_field.grid(column=2, row=1, columnspan=2, sticky=W)

btn = Button(window, text="Generuj stronę", command=generate, width=35)
btn.grid(column=0, row=6, columnspan=5, pady=5)

progress = Progressbar(window, length=400)
progress.grid(column=0, row=7, columnspan=4, sticky=W)

window.mainloop()